---  # ASE Gitlab CI configuration

services:
  - postgres:latest
  - mysql:latest
  - mariadb:latest

variables:
  POSTGRES_DB: testase
  POSTGRES_USER: ase
  POSTGRES_PASSWORD: "ase"
  MYSQL_DATABASE: testase_mysql
  MYSQL_ROOT_PASSWORD: ase

# Check oldest supported Python with oldest supported libraries.
# Does not install any optional libraries except matplotlib.
# This test does not use the --strict flag because things like
# deprecation warnings may be rampant, yet these are only important
# for the future, not the past.
#
# Py3.5 wheels exist from scipy-0.16.x, July 2015.  We choose scipy-0.17.1.
# Consistent with numpy-1.10.x (October 2015).  We choose 1.10.4.
python_3_oldlibs_tests:
  image: python:3.5-slim
  script:
    - pip install numpy==1.10.4 scipy==0.17.1 matplotlib==2.0.0
    - pip install --no-deps .
    - python --version
    - ase test

# Check newest Python with all the standard dependencies at newest versions.
#
# Scipy does not compile on 3.8 image.  So using 3.7-slim (askhl, 2019-10-16)
# We currently use scipy 1.2.1 because of failures with 1.3.0 on py3.7.
# TODO: Make things work with 1.3.0
# psycopg2-binary is for testing the postgres backend for ase.db
python_3_tests:
  image: python:3.7-slim
  script:
    - pip install pyflakes psycopg2-binary netCDF4 pymysql cryptography
    - pip install scipy==1.2.1  # TODO: Delete me
    - pip install .
    - python --version
    - ase test --strict
    - cd $CI_PROJECT_DIR
    - python -We:invalid -m compileall -f -q ase/

# Currently (2019-07-26) facing a problem where PATH does not include
# /opt/conda/bin.  So we manually update the PATH.  This could be a temporary
# issue with the gitlab runners.
#
# Same scipy versioning issue as for the ordinary tests.
#
# Grrr.  Failing after image updates, or something.  Disabling.
# --askhl 2019-10-16
.conda_tests:
  image: continuumio/anaconda3
  script:
    - apt-get update
    - echo $PATH
    - export PATH=/opt/conda/bin:$PATH
    - conda install -yq pip wheel numpy scipy==1.2.1 matplotlib flask
    - pip install .
    - python --version
    - ase test --strict

# Should be same image as python_3_tests except
# not slim because then we don't have tk
docs_test:
  image: python:3.7
  script:
    - pip install .[docs]
    - ase info
    - which sphinx-build
    - cd $CI_PROJECT_DIR/doc
    - python -m ase.utils.sphinx run  # test scripts
    - sphinx-build -W . build

distribution_package:
  image: python:3.7
  script:
    - apt-get update
    - apt-get install -y gettext  # For compiling ase gui translations
    - mkdir dist
    - python setup.py sdist | tee dist/setup_sdist.log
    - python setup.py bdist_wheel | tee dist/setup_bdist_wheel.log
    - pip install dist/ase-*.tar.gz
    - ase test
    - pip uninstall --yes ase
    - pip install dist/ase-*-py3-none-any.whl
    - ase test
  artifacts:
    paths:
      - dist
    expire_in: 1 week
  when: manual
